1  Directory archives
=====================

The following steps are necessary to evaluate Tor directory data.

See report/ for a more detailed description on measurements and results.

Unless otherwise stated, all operations are expected to be executed in the
project root.


1.0  Requirements
-----------------

- Directory archives: These scripts parse and evaluate directory archives
  as created by Peter Palfrader's directory-archive script. See the
  following link for details:

  https://svn.torproject.org/svn/tor/trunk/contrib/directory-archive/

  The directory archives since May 2004 can also be downloaded at:

  http://archive.torproject.org/tor-directory-authority-archive/

- Java 6 (sudo apt-get install sun-java6-jdk)

- PostgreSQL (sudo apt-get install postgresql)

- GNU R (sudo apt-get install r-base)


1.1  Populate directory database
--------------------------------

Place directory archives where they are expected. You'll need the files
consensuses-*.tar.bz2, server-descriptors-*.tar.bz2, and
extra-infos-*.tar.bz2. You don't need to copy these files but can move
them, as they are not deleted during the process. Make sure that
enough disk space is available; e.g., the archives for all of 2008 (plus
descriptors of december 2007) are 6.5G compressed and 13G unpacked in
size. Speaking of, it's useful to have one month more of descriptors
(router and extra-info) than of consensuses, because otherwise the
first consensuses would refer to unknown descriptors.

$ mkdir archives/
$ mv <some path>/*.tar.bz2 archives/

Extract archives to separate data/dirarch/ directory:

$ mkdir data/
$ mkdir data/dirarch/
$ scripts/dirarch/extract-dirarch.sh

Prepare PostgreSQL database by creating a database user and the
descriptor database (throughout this howto, the database user is
'postgres' with password 'metriX'):

$ sudo -u postgres psql template1
template1=# alter user postgres with encrypted password 'metriX';
template1=# \q
$ sudo -u postgres createdb descriptors

Compile the Java classes:

$ javac -d bin/ -cp src/:lib/* src/org/torproject/metrics/dirarch/*.java

Run the database import application. Note that when performing the initial
database import while writing this howto the JVM broke several times! As a
workaround, only small portions of the data could be important at a time.
This doesn't pose a serious problem, as duplicates are safely ignored. The
final import can therefore consist of all files to check if any data are
missing. (The line break here is for formatting purposes only; run
ParseDirectoryData class without parameters to learn about usage.)

$ java -cp bin/:lib/* org.torproject.metrics.dirarch.ParseDirectoryData
  data/dirarch/ descriptors metriX

You can now check what data the database contains, e.g., how many router
descriptors there are:

$ sudo -u postgres psql descriptors
descriptors=# SELECT COUNT(*) FROM descriptor;
descriptors=# \q

The database is now populated with data.


1.2  Evaluate database contents and generate diagrams
-----------------------------------------------------

After importing directory archives into the database, they can be
evaluated using a second Java application.

In case you changed anything in the Java classes, recompile them now:

$ javac -d bin/ -cp src/:lib/* src/org/torproject/metrics/dirarch/*.java

The evaluation application can be run either as a whole or by selecting a
single evaluation only. Running all evaluations is done with this command:

$ java -cp bin/:lib/* org.torproject.metrics.dirarch.EvaluateDirectoryData
  descriptors metriX out/dirarch/

Single evaluations are selected by adding a number from the following list
as fourth parameter, e.g.:

$ java -cp bin/:lib/* org.torproject.metrics.dirarch.EvaluateDirectoryData
  descriptors metriX out/dirarch/ 2

The following evaluations are available:

0. Data sanity check
1. Network status consensus flags 
2. Relay versions
3. Observed bandwidth vs. {read|write}history
4. Relays on dynamic IP addresses
5. Relays by country
6. Advertised bandwidth by country
7. Bandwidth history by country

Every evaluation (besides the sanity check) writes a single output file to
the out/dirarch/ directory. These output files are comma-separated-value
files which can be used as input for prepared R scripts. The result will be
PDF diagrams that can be used in reports. The following call executes the
R scripts:

$ mkdir report/
$ mkdir report/dirarch/
$ R --no-save -q < scripts/dirarch/dirarch.R


1.3 Evaluation of alternative relay flag requirements
-----------------------------------------------------

The directory archive database can be used for more evaluations than
network metrics. One example is the evaluation of alternative requirements
for Fast, Stable, and Guard flags.

This evaluation requires the results of the relayflags evaluation as
described in 1.2 in order to compare in how far simulation results reflect
reality.

Before running the application, you should make sure that the data set and
the fraction of evaluated relays are reasonably small. An evaluation of 16
months of data with a sample size of 1/8 of all relays took roughly 3:30
hours and was CPU-bound on a moderately fast server.

Start with compiling the required Java classes:

$ javac -d bin/ -cp src/:lib/* src/org/torproject/metrics/dirarch/*.java

Run the application with the following or similar parameters:

$ java -cp bin/:lib/*
  org.torproject.metrics.dirarch.AlternativeFlagRequirements
  descriptors metriX out/dirarch/

In case you change the source code, e.g., to include a smaller or larger
sample of relays in the evaluation, recompile and run the application with
an additional parameter, so that an evaluation table is rewritten:

$ java -cp bin/:lib/*
  org.torproject.metrics.dirarch.AlternativeFlagRequirements
  --create-evaluation-table
  descriptors metriX out/dirarch/

Generate graphs using the following R script:

$ mkdir report/
$ mkdir report/dirarch/
$ R --no-save -q < scripts/dirarch/flagrequirements.R


2  Directory requests
=====================


2.1  Parse geoip-stats files
----------------------------

Put the geoip-stats files in a directory data/geoipstats/, giving them
arbitrary filenames to identify the routers later on. Router nicknames are
probably a fine choice.

$ make data/
$ make data/geoipstats/

Compile the parsing script.

$ javac -d bin/ -cp src/:lib/* src/org/torproject/metrics/dirreq/*.java

Run the parsing script:

$ java -cp bin/:lib/* org.torproject.metrics.dirreq.ParseGeoipStats
  data/geoipstats/ out/geoipstats/

$ mkdir report/
$ mkdir report/dirreq/
$ R -q --no-save < scripts/dirreq/geoipstats.R


2.2  Check how reliable directory mirrors are
---------------------------------------------

Compile the checking application:

$ javac -d bin/ -cp src/:lib/* src/org/torproject/metrics/dirreq/*.java

Run the parsing script:

$ java -cp bin/:lib/* org.torproject.metrics.dirreq.CheckDirectoryMirrors
  out/checkdirectories/


3  Bridge archives
==================

Note: In order to parse bridge archives it is necessary to have a populated
database of descriptor archives. See 1.1 for details. The reason is that some
bridges publish both bridge and relay descriptors or have been switched from
relay operation to bridge operation. These bridges have seen far more clients
and bandwidth usage than other bridges and would make the results unusable.
The current approach is to remove those bridges from the statistics that have
published a descriptor as relays in the past 7 days before publishing one as
bridge. The rationale is that bridges will forget about clients they have
seen as relays after this time. In fact, 7 days may be too long, but it is
better to err on the safe side. This exclusion can only take place with the
database of relay descriptors.

Put the bridge archives in a directory bridges/ and execute the extraction
script:

$ mkdir data/
$ mkdir data/bridges
$ scripts/bridges/extract-bridges.sh

$ javac -d bin/ -cp src/:lib/* src/org/torproject/metrics/bridges/*.java

Run the parsing script:

$ java -Xms128m -Xmx1024m -cp bin/:lib/*
  org.torproject.metrics.bridges.ParseBridgeData data/bridges/ descriptors
  metriX out/bridges/


4  Performance data
===================

There are two types of performance data which can be evaluated using these
scripts. The first type is speedracer data, the second type buildcircuits
data. For more information about generating these data, see the TorFlow
project.


4.1  Speedracer data
--------------------

Copy (or move, the files are not modified during evaluation) the output of
a speedracer run to data/speedraces/ .

Compile the Java classes that are required for evaluation:

$ javac -d bin/ -cp src/:lib/*
  src/org/torproject/metrics/performance/*.java

Run the parsing script:

$ java -cp bin/:lib/*
  org.torproject.metrics.performance.ParseSpeedracerStats data/speedraces/
  out/performance/


4.2  torperf data
-----------------

Set up measurements as described in:

https://svn.torproject.org/svn/torperf/trunk/measurements-HOWTO

Put the resulting *.data files in a directory data/torperf/ .

Run the R script:

$ R -q --no-save < scripts/torperf/torperf.R

Compile the PDF using pdflatex:

$ cd report/performance/
$ pdflatex torperf.tex       # Run twice to update references


5  Entry-guard statistics
=========================

Put the entry-stats files in a directory data/entrystats/, giving them
arbitrary filenames to identify the routers later on. Router nicknames are
probably a fine choice.

$ make data/
$ make data/entrystats/

Compile the parsing app:

$ javac -d bin/ -cp src/:lib/* src/org/torproject/metrics/entry/*.java

Run the parsing script:

$ java -cp bin/:lib/* org.torproject.metrics.entry.ParseEntryStats
  data/entrystats/ out/entrystats/


6  Exit port statistics
=======================

Put the exit-stats file coming from one exit node containing one or more
days of measurements with possibly different exit policies in a directory
data/exit/exit-stats . Put the network status consensuses covering the
whole measurement interval in a directory called data/exit/consensuses/ . 

Compile the evaluation application:

$ javac -d bin/ -cp src/:lib/* src/org/torproject/metrics/exit/*.java

Run the evaluation application:

$ java -cp bin/:lib/* org.torproject.metrics.exit.EvaluateExitPortStats
  data/exit/exit-stats data/exit/consensuses/
  cGyhQdOhBYtSUJtKG2sovzWuWg8,h7m2jUhDHih5WvZe6nGEgDzO1xU out/exit/

The four parameters are:

1. the exit-stats file,
2. the directory containing consensuses
3. a comma-separated list of identities of the measuring relay, and
4. the output directory.


7  Cell statistics
==================

Put the buffer-stats files in a directory data/buffer/ , renaming them to
the relay names that shall appear in the output graphs.

$ make data/
$ make data/buffer/

Compile the evaluation application:

$ javac -d bin/ -cp src/:lib/* src/org/torproject/metrics/buffer/*.java

Run the evaluation application:

$ java -cp bin/:lib/* org.torproject.metrics.buffer.EvaluateCellStats
  data/buffer/ out/buffer/

Run the R script:

$ R --no-save -q < scripts/buffer/cellstats.R

Compile the PDF using pdflatex:

$ cd report/buffer/
$ pdflatex torperf.tex       # Run twice to update references

