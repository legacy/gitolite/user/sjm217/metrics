all: run

build:
	scalac src/org/torproject/metrics/autopromotion/*.scala

run:
	JAVA_OPTS=-Xmx1024M scala org.torproject.metrics.autopromotion.AutoPromotion ../bridge-stability/bridge-descriptors-2010-02/statuses | tee out.txt
