#!/bin/sh
for i in archives/consensuses-*.tar.bz2; do
  echo "Extracting $i..."; tar -C data/dirarch/ --exclude "*-vote-*" -xf $i; done
for i in archives/server-descriptors-*.tar.bz2; do
  echo "Extracting $i..."; tar -C data/dirarch/ -xf $i; done
for i in archives/extra-infos-*.tar.bz2; do
  echo "Extracting $i..."; tar -C data/dirarch/ -xf $i; done
for i in archives/statuses-*.tar.bz2; do
  echo "Extracting $i..."; tar -C data/dirarch/ -xf $i; done

