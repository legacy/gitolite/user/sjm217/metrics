/* Copyright 2009 Karsten Loesing
 * See LICENSE for licensing information */
package org.torproject.metrics.performance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public final class ParseSpeedracerStats {

    private ParseSpeedracerStats() {
    }

    public static void main(final String[] args) throws Exception {
        if (args.length < 2) {
            System.err.println("Usage: java "
                    + ParseSpeedracerStats.class.getSimpleName()
                    + " <speedracer data directory> <output directory>");
            System.exit(1);
        }
        File dataDir = new File(args[0]);
        if (!dataDir.exists() || !dataDir.isDirectory()) {
            System.err.println(dataDir.getAbsolutePath()
                    + " does not exist or is not a directory.");
            System.exit(1);
        }
        File outputDirectory = new File(args[1]);
        if (outputDirectory.exists() && !outputDirectory.isDirectory()) {
            System.err.println(outputDirectory.getAbsolutePath()
                    + " exists, but is not a directory.");
            System.exit(1);
        }
        outputDirectory.mkdirs();
        File fileOut = new File(outputDirectory.getAbsolutePath()
            + File.separatorChar + "speed.csv");
        BufferedWriter out = new BufferedWriter(new FileWriter(fileOut,
                false));
        out.write("slice,node,CC,SC,EB,BD,BR\n");

        System.out.println("Parsing Speedracer stats in "
                + dataDir.getAbsolutePath());
        SortedMap<String, double[]> observed = new TreeMap<String, double[]>();
        for (File file : dataDir.listFiles()) {
            String filename = file.getName();
            String[] fnparts = filename.split("-");
            if (fnparts.length != 7) {
                continue;
            }
            if (!fnparts[0].equals("stats")) {
                continue;
            }
            String from = fnparts[1].split(":")[0];
            String to = fnparts[1].split(":")[1];
            String runs = fnparts[2];

            if (!runs.equals("250")) {
                continue;
            }

            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = null, line0 = null, line1 = null;
            boolean parse = false;
            while ((line = br.readLine()) != null) {
                if (line.contains("----- Bandwidth Ratios -----")) {
                    parse = true;
                }
                if (line.contains("----- Failed Counts -----")) {
                    break;
                }
                if (!parse) {
                    continue;
                }
                if (!line.startsWith("   ")) {
                    line0 = line;
                } else if (line.startsWith("   CC")) {
                    line1 = line;
                } else if (line.startsWith("   ET")) {
                    String node = line0.split(" ")[0];
                    String[] lnparts = line1.substring(3).split(" ");
                    int circuitsChosen = Integer.parseInt(
                            lnparts[0].split("=")[1]);
                    int streamsChosen = Integer.parseInt(
                            lnparts[3].split("=")[1]);
                    lnparts = line.substring(3).split(" ");
                    double meanBW = Double.parseDouble(
                            lnparts[1].split("=")[1]);
                    double sdBW = Double.parseDouble(lnparts[2].split("=")[1]);
                    double ratio = Double.parseDouble(
                            lnparts[5].split("=")[1]);
                    out.write(from + "," + node + "," + circuitsChosen
                            + "," + streamsChosen + "," + meanBW + ","
                            + sdBW + "," + ratio + "\n");
                    double[] t = null;
                    if (observed.containsKey(node)) {
                        t = observed.remove(node);
                    } else {
                        t = new double[2];
                    }
                    t[0] += circuitsChosen;
                    t[1] += ratio;
                    observed.put(node, t);
                }
            }
        }
        out.close();
        fileOut = new File(outputDirectory.getAbsolutePath()
                        + File.separatorChar + "speed2.csv");
                out = new BufferedWriter(new FileWriter(fileOut, false));
                out.write("node,CCsum,BRsum\n");
        for (Map.Entry<String, double[]> e : observed.entrySet()) {
            out.write(e.getKey() + "," + e.getValue()[0] + ","
                    + e.getValue()[1] + "\n");
        }
        out.close();
    }
}
