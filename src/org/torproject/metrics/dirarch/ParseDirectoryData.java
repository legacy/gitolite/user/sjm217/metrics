/* Copyright 2008-2009 Karsten Loesing
 * See LICENSE for licensing information */
package org.torproject.metrics.dirarch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Stack;
import java.util.TimeZone;
import com.maxmind.geoip.LookupService;
import org.torproject.metrics.util.Crypto;
import org.torproject.metrics.util.Encoding;

/**
 * Parse directory data.
 */
public final class ParseDirectoryData {

    private ParseDirectoryData() {
    }

    public static void main(final String[] args) throws Exception {

        long startParsing = System.currentTimeMillis();

        if (args.length < 3) {
            System.err.println("Usage: java "
                    + ParseDirectoryData.class.getSimpleName()
                    + " <path to data/ directory> <database name> "
                    + "<database password>");
            System.exit(1);
        }
        File testenvDirectory = new File(args[0]);
        if (!testenvDirectory.exists() || !testenvDirectory.isDirectory()) {
            System.err.println(testenvDirectory.getAbsolutePath()
                    + " does not exist or is not a directory.");
            System.exit(1);
        }
        String dbName = args[1];
        System.out.println("Parsing " + testenvDirectory.getAbsolutePath());

        // connect to database
        String connectionURL = "jdbc:postgresql:" + dbName;
        Connection conn = DriverManager.getConnection(connectionURL,
                "postgres", args[2]);

        // create tables if required
        boolean createRouterTable = true, createDescriptorTable = true,
                createExtrainfoTable = true;
        ResultSet resultSet = conn.getMetaData().getTables("%", "%", "%",
                new String[] { "TABLE" });
        while (resultSet.next()) {
            String tableName = resultSet.getString("TABLE_NAME");
            if (tableName.equalsIgnoreCase("statusentry")) {
                createRouterTable = false;
            } else if (tableName.equalsIgnoreCase("descriptor")) {
                createDescriptorTable = false;
            } else if (tableName.equalsIgnoreCase("extrainfo")) {
                createExtrainfoTable = false;
            }
        }
        resultSet.close();
        Statement statement = conn.createStatement();
        if (createRouterTable) {
            statement.execute("CREATE TABLE statusentry ("
                    + "validafter TIMESTAMP NOT NULL, "
                    + "descriptor CHAR(40) NOT NULL, "
                    + "authority CHAR NOT NULL, badexit CHAR NOT NULL, "
                    + "baddirectory CHAR NOT NULL, exit CHAR NOT NULL, "
                    + "fast CHAR NOT NULL, guard CHAR NOT NULL, "
                    + "hsdir CHAR NOT NULL, named CHAR NOT NULL, "
                    + "stable CHAR NOT NULL, running CHAR NOT NULL, "
                    + "unnamed CHAR NOT NULL, valid CHAR NOT NULL, "
                    + "v2dir CHAR NOT NULL, v3dir CHAR NOT NULL,"
                    + "PRIMARY KEY (validafter, descriptor))");
        }
        if (createDescriptorTable) {
            statement.execute("create table descriptor ("
                    + "nickname VARCHAR(19) NOT NULL, "
                    + "address VARCHAR(15) NOT NULL, "
                    + "orport INTEGER NOT NULL, "
                    + "dirport INTEGER NOT NULL, "
                    + "bandwidthavg BIGINT NOT NULL, "
                    + "bandwidthburst BIGINT NOT NULL, "
                    + "bandwidthobserved BIGINT NOT NULL, "
                    + "platform VARCHAR(256), "
                    + "published TIMESTAMP NOT NULL, "
                    + "fingerprint CHAR(40) NOT NULL, "
                    + "hibernating CHAR, uptime BIGINT, "
                    + "readhistoryavg BIGINT, writehistoryavg BIGINT, "
                    + "extrainfo CHAR(40), "
                    + "hiddenservicedir CHAR, "
                    + "descriptor CHAR(40) NOT NULL PRIMARY KEY, "
                    + "country CHAR(2))");
        }
        if (createExtrainfoTable) {
            statement.execute("CREATE TABLE extrainfo ("
                    + "readhistoryavgex BIGINT, "
                    + "writehistoryavgex BIGINT, "
                    + "extrainfo CHAR(40) NOT NULL PRIMARY KEY)");
        }

        // create indexes if required
        boolean createDescaddr = true;
        resultSet = conn.getMetaData().getIndexInfo(null, null,
                "descriptor", false, true);
        while (resultSet.next()) {
            String indexName = resultSet.getString("INDEX_NAME");
            if (indexName.equalsIgnoreCase("descaddr")) {
                createDescaddr = false;
            }
        }
        boolean createRouterdesc = true, createRoutervalid = true;
        resultSet = conn.getMetaData().getIndexInfo(null, null,
                "statusentry", false, true);
        while (resultSet.next()) {
            String indexName = resultSet.getString("INDEX_NAME");
            if (indexName.equalsIgnoreCase("routerdesc")) {
                createRouterdesc = false;
            } else if (indexName.equalsIgnoreCase("routervalid")) {
                createRoutervalid = false;
            }
        }
        resultSet.close();
        if (createDescaddr) {
            statement.execute("CREATE INDEX descaddr ON descriptor "
                    + "(address)");
        }
        if (createRouterdesc) {
            statement.execute("CREATE INDEX routerdesc ON statusentry "
                    + "(descriptor)");
        }
        if (createRoutervalid) {
            statement.execute("CREATE INDEX routervalid ON "
                    + "statusentry (validafter)");
        }

        // prepare statements
        PreparedStatement psRs = conn.prepareStatement("SELECT COUNT(*) FROM "
                + "statusentry WHERE validafter = ?");
        PreparedStatement psDs = conn.prepareStatement("SELECT COUNT(*) FROM "
                + "descriptor WHERE descriptor = ?");
        PreparedStatement psEs = conn.prepareStatement("SELECT COUNT(*) FROM "
                + "extrainfo WHERE extrainfo = ?");
        PreparedStatement psR = conn.prepareStatement("INSERT INTO "
                + "statusentry (validafter, descriptor, authority, badexit, "
                + "baddirectory, exit, fast, guard, hsdir, named, stable, "
                + "running, unnamed, valid, v2dir, v3dir) VALUES (?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        PreparedStatement psD = conn.prepareStatement("INSERT "
                + "INTO descriptor (nickname, address, orport, dirport, "
                + "bandwidthavg, bandwidthburst, bandwidthobserved, platform, "
                + "published, fingerprint, hibernating, uptime, "
                + "readhistoryavg, writehistoryavg, extrainfo, "
                + "hiddenservicedir, descriptor, country) VALUES (?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        PreparedStatement psE = conn.prepareStatement("INSERT "
                + "INTO extrainfo ("
                + "readhistoryavgex, writehistoryavgex, "
                + "extrainfo) VALUES (?, ?, ?)");

        SimpleDateFormat timeFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        // initialize geoip lookup
        String dbfile = "res/GeoIP.dat";
        LookupService cl = new LookupService(dbfile,
                LookupService.GEOIP_MEMORY_CACHE);

        Stack<File> directoriesLeftToParse = new Stack<File>();
        directoriesLeftToParse.push(testenvDirectory);
        while (!directoriesLeftToParse.isEmpty()) {

            // if this is a directory, put all contained files in stack and
            // continue
            File directoryOrFile = directoriesLeftToParse.pop();
            if (directoryOrFile.isDirectory()) {
                for (File fileInDir : directoryOrFile.listFiles()) {
                    directoriesLeftToParse.push(fileInDir);
                }
                continue;
            }

            try {
                BufferedReader br = new BufferedReader(new FileReader(
                        directoryOrFile));
                String line = br.readLine();
                if (line == null) {
                    // empty file. skip.
                } else if (line.startsWith("network-status-version ")) {
                    Timestamp validAfter = null;
                    String rLine = null;
                    boolean dontAdd = false;
                    do {
                        if (line.startsWith("valid-after ") || // v3
                                line.startsWith("published ")) { // v2
                            int cut = line.startsWith("published ") ?
                                    10 : 12;
                            validAfter = new Timestamp(timeFormat.parse(
                                    line.substring(cut)).getTime());
                            psRs.setTimestamp(1, validAfter, cal);
                            ResultSet rs = psRs.executeQuery();
                            rs.next();
                            if (rs.getInt(1) > 0) {
                                dontAdd = true;
                                break;
                            }
                        } else if (line.startsWith("r ")) {
                            rLine = line;
                        } else if (line.startsWith("s ")) {
                            String[] split = rLine.split(" ");
                            psR.setTimestamp(1, validAfter, cal);
                                    // NETWORKSTATUS
                            psR.setString(2, Encoding.toHex(Encoding
                                    .fromBase64(split[3] + "=")));
                                    // DESCRIPTOR
                            psR.setString(3, line.contains(" Authority") ? "1"
                                    : "0");
                            psR.setString(4, line.contains(" BadExit") ? "1"
                                    : "0");
                            psR.setString(5, line.contains(" BadDirectory")
                                    ? "1" : "0");
                            psR.setString(6, line.contains(" Exit") ? "1"
                                    : "0");
                            psR.setString(7, line.contains(" Fast") ? "1"
                                    : "0");
                            psR.setString(8, line.contains(" Guard") ? "1"
                                    : "0");
                            psR.setString(9, line.contains(" HSDir") ? "1"
                                    : "0");
                            psR.setString(10, line.contains(" Named") ? "1"
                                    : "0");
                            psR.setString(11, line.contains(" Stable") ? "1"
                                    : "0");
                            psR.setString(12, line.contains(" Running") ? "1"
                                    : "0");
                            psR.setString(13, line.contains(" Unnamed") ? "1"
                                    : "0");
                            psR.setString(14, line.contains(" Valid") ? "1"
                                    : "0");
                            psR.setString(15, line.contains(" V2Dir") ? "1"
                                    : "0");
                            psR.setString(16, line.contains(" V3Dir") ? "1"
                                    : "0");
                            psR.addBatch();
                        }
                    } while ((line = br.readLine()) != null);
                    if (!dontAdd) {
                        psR.executeBatch();
                    }
                } else if (line.startsWith("router ")) {
                    String platform = null;
                    int hibernating = -1, hiddenServiceDir = -1;
                    long readHistoryAvg = -1L, writeHistoryAvg = -1L;
                    String extraInfoDigest = null;
                    do {
                        String lineTmp = line;
                        if (lineTmp.startsWith("opt ")) {
                            lineTmp = lineTmp.substring(4);
                        }
                        if (lineTmp.startsWith("@")) {
                            continue;
                        } else if (lineTmp.startsWith("router ")) {
                            String[] parts = lineTmp.split(" ");
                            psD.setString(1, parts[1]); // NICKNAME
                            psD.setString(2, parts[2]); // IP
                            psD.setString(18, cl.getCountry(parts[2])
                                    .getCode()); // COUNTRY
                            psD.setInt(3, Integer.parseInt(parts[3]));
                                    // ORPORT
                            psD.setInt(4, Integer.parseInt(parts[4]));
                                    // DIRPORT
                        } else if (lineTmp.startsWith("platform ")) {
                            platform = lineTmp.substring(9);
                        } else if (lineTmp.startsWith("published ")) {
                            psD.setTimestamp(9, new Timestamp(timeFormat.parse(
                                    lineTmp.substring(10)).getTime()), cal);
                                    // PUBLISHED
                        } else if (lineTmp.startsWith("fingerprint ")) {
                            psD.setString(10, lineTmp
                                    .substring(12).replace(" ", ""));
                                    // FINGERPRINT
                        } else if (lineTmp.startsWith("uptime ")) {
                            psD.setLong(12, Long.parseLong(lineTmp
                                    .substring(7))); // UPTIME
                        } else if (lineTmp.startsWith("bandwidth ")) {
                            String[] parts = lineTmp.split(" ");
                            psD.setLong(5, Long.parseLong(parts[1]));
                                    // BANDWIDTHAVG
                            psD.setLong(6, Long.parseLong(parts[2]));
                                    // BANDWIDTHBURST
                            psD.setLong(7, Long.parseLong(parts[3]));
                                    // BANDWIDTHOBSERVED
                        } else if (lineTmp.startsWith("extra-info-digest ")) {
                            extraInfoDigest = lineTmp.substring(18);
                        } else if (lineTmp.startsWith("read-history ")) {
                            String[] split = lineTmp.replace("  ", " ")
                                    .split(" ");
                            if (split.length > 5) {
                                String[] vals = split[5].split(",");
                                long num = 0, sum = 0;
                                for (int i = 0; i < vals.length; i++) {
                                    num++;
                                    sum += (Long.parseLong(vals[i]));
                                }
                                readHistoryAvg = sum / num;
                            }
                        } else if (lineTmp.startsWith("write-history ")) {
                            String[] split = lineTmp.split(" ");
                            if (split.length > 5) {
                                String[] vals = split[5].split(",");
                                long num = 0, sum = 0;
                                for (int i = 0; i < vals.length; i++) {
                                    num++;
                                    sum += (Long.parseLong(vals[i]));
                                }
                                writeHistoryAvg = sum / num;
                            }
                        } else if (lineTmp.startsWith("hidden-service-dir ")) {
                            hiddenServiceDir = Integer.parseInt(lineTmp
                                    .substring(19));
                        } else if (lineTmp.startsWith("hibernating ")) {
                            hibernating = Integer.parseInt(lineTmp
                                    .substring(12));
                        } else if (lineTmp.startsWith("router-signature")) {
                            String descriptor = directoryOrFile.getName()
                                    .toUpperCase();
                            psD.setString(17, descriptor);
                            psD.setString(8, platform);
                            if (hibernating >= 0) {
                                psD.setInt(11, hibernating);
                            } else {
                                psD.setNull(11, Types.INTEGER);
                            }
                            if (readHistoryAvg >= 0) {
                                psD.setLong(13, readHistoryAvg);
                            } else {
                                psD.setNull(13, Types.BIGINT);
                            }
                            if (writeHistoryAvg >= 0) {
                                psD.setLong(14, writeHistoryAvg);
                            } else {
                                psD.setNull(14, Types.BIGINT);
                            }
                            psD.setString(15, extraInfoDigest);
                            psD.setString(16,
                                    hiddenServiceDir >= 0 ? hiddenServiceDir
                                            + "" : null);
                            platform = null;
                            hibernating = -1;
                            readHistoryAvg = -1;
                            writeHistoryAvg = -1;
                            extraInfoDigest = null;
                            hiddenServiceDir = -1;
                            psDs.setString(1, descriptor);
                            ResultSet rs = psDs.executeQuery();
                            rs.next();
                            if (rs.getInt(1) > 0) {
                                psD.clearParameters();
                            } else {
                                psD.executeUpdate();
                            }
                        }
                    } while ((line = br.readLine()) != null);
                } else if (line.startsWith("extra-info ")) {
                    long readHistoryAvg = -1L, writeHistoryAvg = -1L;
                    do {
                        if (line.startsWith("@")) {
                            continue;
                        } else if (line.startsWith("read-history ")) {
                            String[] split = line.split(" ");
                            if (split.length > 5) {
                                String[] vals = split[5].split(",");
                                long num = 0, sum = 0;
                                for (int i = 0; i < vals.length; i++) {
                                    num++;
                                    sum += (Long.parseLong(vals[i]));
                                }
                                readHistoryAvg = sum / num;
                            }
                        } else if (line.startsWith("write-history ")) {
                            String[] split = line.split(" ");
                            if (split.length > 5) {
                                String[] vals = split[5].split(",");
                                long num = 0, sum = 0;
                                for (int i = 0; i < vals.length; i++) {
                                    num++;
                                    sum += (Long.parseLong(vals[i]));
                                }
                                writeHistoryAvg = sum / num;
                            }
                        } else if (line.startsWith("router-signature")) {
                            String extrainfo = directoryOrFile.getName()
                                    .toUpperCase();
                            psE.setString(3, extrainfo); // EXTRAINFO
                            if (readHistoryAvg >= 0) {
                                psE.setLong(1, readHistoryAvg);
                            } else {
                                psE.setNull(1, Types.BIGINT); // READHISTORYAVG
                            }
                            if (writeHistoryAvg >= 0) {
                                psE.setLong(2, writeHistoryAvg);
                            } else {
                                psE.setNull(2, Types.BIGINT); // WRITEHISTORYAVG
                            }
                            readHistoryAvg = -1;
                            writeHistoryAvg = -1;
                            psEs.setString(1, extrainfo);
                            ResultSet rs = psEs.executeQuery();
                            rs.next();
                            if (rs.getInt(1) > 0) {
                                psE.clearParameters();
                            } else {
                                psE.executeUpdate();
                            }
                        }
                    } while ((line = br.readLine()) != null);
                }
                br.close();
            } catch (Exception e) {
                System.out.println("Exception while parsing "
                        + directoryOrFile.getAbsolutePath() + ". Exiting.");
                e.printStackTrace();
                break;
            }
        }
        long doneParsing = System.currentTimeMillis();
        System.out.println("Parsing finished after "
                + (doneParsing - startParsing) / 1000 + " seconds.");
        conn.close();
    }
}

