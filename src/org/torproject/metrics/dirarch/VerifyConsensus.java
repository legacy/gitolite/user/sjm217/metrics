package org.torproject.metrics.dirarch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.StringReader;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.RSAPublicKeySpec;
import java.util.HashSet;
import java.util.Set;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.provider.JCERSAPublicKey;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PEMWriter;
import org.torproject.metrics.util.Encoding;

public class VerifyConsensus {

	private VerifyConsensus() {
	}

	public static void main(final String[] args) throws Exception {
		java.security.Security.addProvider(new BouncyCastleProvider());
		if (args.length < 2) {
			System.err.println("Usage: java "
					+ VerifyConsensus.class.getSimpleName()
					+ " <path to data/ directory> <cached certs>");
			System.exit(1);
		}
		File testenvDirectory = new File(args[0]);
		if (!testenvDirectory.exists() || !testenvDirectory.isDirectory()) {
			System.err.println(testenvDirectory.getAbsolutePath()
					+ " does not exist or is not a directory.");
			System.exit(1);
		}
		File cachedCerts = new File(args[1]);
		if (!cachedCerts.exists() || cachedCerts.isDirectory()) {
			System.err.println(testenvDirectory.getAbsolutePath()
					+ " does not exist or is a directory.");
			System.exit(1);
		}

		// {
		// KeyFactory keyFactory = KeyFactory.getInstance("RSA", "BC");
		// RSAPublicKeySpec pubKeySpec = new RSAPublicKeySpec(new BigInteger(
		// "d46f473a2d746537de2056ae3092c451", 16), new BigInteger(
		// "11", 16));
		// RSAPublicKey pubKey = (RSAPublicKey) keyFactory
		// .generatePublic(pubKeySpec);
		//
		// }

		// parse certs
		BufferedReader br = new BufferedReader(new FileReader(cachedCerts));
		// String fingerprint = null;
		// RSAPublicKeyStructure identityKey = null;
		Set<PublicKey> signingKeys = new HashSet<PublicKey>();
		// RSAPublicKeyStructure certification = null;
		String line;
		while ((line = br.readLine()) != null) {
			if (line.startsWith("dir-signing-key")) {
				StringBuilder sb = new StringBuilder("-----BEGIN PUBLIC KEY-----\n");
				line = br.readLine();
				line = br.readLine();
				while (!line.startsWith("-----END")) {
					sb.append(line + "\n");
					line = br.readLine();
				}
				sb.append("-----END PUBLIC KEY-----\n");
				
				PEMReader reader = new PEMReader(
						new StringReader(sb.toString()));
				JCERSAPublicKey jceKey = (JCERSAPublicKey) reader.readObject();

//				RSAPublicKeyStructure pk = new RSAPublicKeyStructure(jceKey
//						.getModulus(), jceKey.getPublicExponent());
				System.out.println(jceKey.getModulus());
				System.out.println(jceKey.getPublicExponent());

				KeyFactory keyFactory = KeyFactory.getInstance("RSA");
				RSAPublicKeySpec keySpec = new RSAPublicKeySpec(jceKey
						.getModulus(), jceKey.getPublicExponent());
				PublicKey pubKey = keyFactory.generatePublic(keySpec);
				signingKeys.add(pubKey);
				PEMWriter writer = new PEMWriter(new FileWriter("out22"));
//				
				writer.writeObject(pubKey);
				writer.close();
				System.exit(1);
				// writer.
			}
		}
		System.out.println("Read " + signingKeys.size() + " signing keys.");

		// parse consensuses
		for (File file : testenvDirectory.listFiles()) {
			System.out.println("File: " + file.getName());
			br = new BufferedReader(new FileReader(file));
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				if (line.startsWith("directory-signature")) {
					break;
				}
				sb.append(line + "\n");
			}
			String consensusWithoutSignatures = sb.toString();
			System.out.println("'"
					+ consensusWithoutSignatures.substring(0, 1000) + "'");
			// byte[] hash =
			// Crypto.getHash(consensusWithoutSignatures.getBytes());
			sb = null;
			int matchingSignatures = 0;
			do {
				if (line.startsWith("directory-signature")) {
					if (sb != null) {
						System.out
								.println("Signature: '" + sb.toString() + "'");
						byte[] signature = Encoding.fromBase64(sb.toString());
						// PEMReader reader = new PEMReader(
						// new StringReader(sb.toString()));
						// System.out.println(reader.readObject().getClass().getName());

						for (PublicKey signingKey : signingKeys) {
							// byte[] signature =
							// Encoding.fromBase64(sb.toString());
//							KeyFactory keyFactory = KeyFactory
//									.getInstance("RSA");
//							RSAPublicKeySpec keySpec = new RSAPublicKeySpec(
//									signingKey.getModulus(), signingKey
//											.getPublicExponent());
//							PublicKey pubKey = keyFactory
//									.generatePublic(keySpec);

							Signature sig = Signature
									.getInstance("SHA1withRSA");
							sig.initVerify(signingKey);
							// sig.update(hash);
							sig.update(consensusWithoutSignatures.getBytes());
							try {
								boolean f = sig.verify(signature);
								System.out.println(f == true ? "Yes" : "No");
							} catch (Exception e) {
								System.out
										.println(" HMMM -> " + e.getMessage());
								e.printStackTrace();
								matchingSignatures++;
								System.out.println("HERE FAILURE "
										+ matchingSignatures);
								System.exit(1);
							}
							// RSAKeyParameters myRSAKeyParameters = new
							// RSAKeyParameters(
							// false, signingKey.getModulus(), signingKey
							// .getPublicExponent());
							// RSAEngine rsa_alg = new RSAEngine();
							// rsa_alg.init(false, myRSAKeyParameters);
							// byte[] decrypted_signature =
							// rsa_alg.processBlock(
							// signature, 0, signature.length);

							// System.out.println(" inpu = " +
							// Encoding.toHexString(input));
							// System.out.println(" hash = " +
							// Encoding.toHexString(hash));
							// System.out.println("");
							// System.out.println(" sign = " +
							// Encoding.toHexString(signature));
							// System.out.println(" decr = "
							// + Encoding.toHexString(decrypted_signature));

							// boolean verified = Encoding.arraysEqual(hash,
							// decrypted_signature);
						}

						// if (runs-- == 0) System.exit(1);
						// JCERSAPublicKey jceKey = (JCERSAPublicKey)
						// reader.readObject();
						// signingKeys.add(new
						// RSAPublicKeyStructure(jceKey.getModulus(),
						// jceKey.getPublicExponent()));

					}
					sb = new StringBuilder();
				} else if (!line.startsWith("-----")) {
					sb.append(line);
				}
				line = br.readLine();
			} while (line != null);
			System.out.println("Matching signatures: " + matchingSignatures);
			System.exit(1);
		}
	}
}
