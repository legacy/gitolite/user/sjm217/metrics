/* Copyright 2008-2009 Karsten Loesing
 * See LICENSE for licensing information */
package org.torproject.metrics.dirarch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public final class EvaluateDirectoryData {

    private EvaluateDirectoryData() {
    }

    public static void main(final String[] args) throws Exception {
        if (args.length < 3) {
            System.err.println("Usage: java "
                    + EvaluateDirectoryData.class.getSimpleName()
                    + " <database name> <database password> <output directory> "
                    + "<evaluation number (optional)>");
            System.exit(1);
        }
        File outputDirectory = new File(args[2]);
        if (outputDirectory.exists() && !outputDirectory.isDirectory()) {
            System.err.println(outputDirectory.getAbsolutePath()
                    + " exists, but is not a directory.");
            System.exit(1);
        }
        outputDirectory.mkdirs();
        BufferedWriter out = null;
        int evaluation = -1;
        if (args.length >= 4) {
            evaluation = Integer.parseInt(args[3]);
        }

        // connect to database
        String connectionURL = "jdbc:postgresql:" + args[0];
        Connection conn = DriverManager.getConnection(connectionURL,
                "postgres", args[1]);
        ResultSet rs = null;
        Statement s = conn.createStatement();
        System.out.println("Connected to database");

        // check data sanity
        if (evaluation == -1 || evaluation == 0) {
            System.out.println("Checking data sanity: "
                    + "The following sanity checks shall help examine whether "
                    + "all data have been imported into the database.");
            long started = System.currentTimeMillis();

            rs = s.executeQuery("SELECT COUNT(*) FROM ("
                + "SELECT COUNT(*) FROM statusentry GROUP BY validafter"
                + ") AS foo");
            rs.next();
            int statuses = rs.getInt("count");
            System.out.println(String.format("1) Database contains %d "
                    + "consensuses. As a rough approximation, these should "
                    + "cover at least %d days. In the 2008 data, there were "
                    + "8703 consensuses covering at least 362 days.",
                    statuses, statuses / 24));

            rs = s.executeQuery("SELECT COUNT(*) FROM ("
                    + "SELECT statusentry.descriptor FROM statusentry "
                    + "LEFT OUTER JOIN descriptor "
                    + "ON statusentry.descriptor = descriptor.descriptor "
                    + "GROUP BY statusentry.descriptor) AS foo");
            rs.next();
            int referencedDescriptors = rs.getInt("count");
            rs = s.executeQuery("SELECT COUNT(*) FROM ("
                    + "SELECT statusentry.descriptor FROM statusentry "
                    + "LEFT OUTER JOIN descriptor "
                    + "ON statusentry.descriptor = descriptor.descriptor "
                    + "WHERE descriptor.descriptor IS NULL "
                    + "GROUP BY statusentry.descriptor) AS foo");
            rs.next();
            int missingDescriptors = rs.getInt("count");
            System.out.println(String.format("2) %d descriptors are "
                    + "referenced from status entries of which %d are missing "
                    + "(%.4f percent). In the 2008 data, 2520 of 1518595 "
                    + "descriptors were missing (0.1659 percent).",
                    referencedDescriptors, missingDescriptors,
                    100.0 * (double) missingDescriptors
                    / (double) referencedDescriptors));

            rs = s.executeQuery("SELECT COUNT(*) FROM ("
                    + "SELECT descriptor.extrainfo FROM statusentry "
                    + "LEFT OUTER JOIN descriptor "
                    + "ON statusentry.descriptor = descriptor.descriptor "
                    + "LEFT OUTER JOIN extrainfo "
                    + "ON descriptor.extrainfo = extrainfo.extrainfo "
                    + "GROUP BY descriptor.extrainfo "
                    + "ORDER BY descriptor.extrainfo) AS foo");
            rs.next();
            int referencedExtrainfos = rs.getInt("count");
            rs = s.executeQuery("SELECT COUNT(*) FROM ("
                    + "SELECT descriptor.extrainfo FROM statusentry "
                    + "LEFT OUTER JOIN descriptor "
                    + "ON statusentry.descriptor = descriptor.descriptor "
                    + "LEFT OUTER JOIN extrainfo "
                    + "ON descriptor.extrainfo = extrainfo.extrainfo "
                    + "WHERE extrainfo.extrainfo IS NULL "
                    + "GROUP BY descriptor.extrainfo) AS foo");
            rs.next();
            int missingExtrainfos = rs.getInt("count");
            System.out.println(String.format("3) %d extra-info documents are "
                    + "referenced from descriptors of which %d are missing "
                    + "(%.4f percent). In the 2008 data, 418 of 449543 "
                    + "extra-info documents were missing (0.0930 percent).",
                    referencedExtrainfos, missingExtrainfos,
                    100.0 * (double) missingExtrainfos
                    / (double) referencedExtrainfos));

            System.out.println("Sanity check finished after "
                + ((System.currentTimeMillis() - started) / 1000)
                + " seconds.");
        }

        // how many consensuses do we have per day (we need this information for
        // all evaluations below)
        SortedMap<String, Integer> consensusesPerDay = null;
        if (evaluation != 0) {
            System.out.print("Counting consensuses per day... ");
            long started = System.currentTimeMillis();
            consensusesPerDay = new TreeMap<String, Integer>();
            rs = s.executeQuery("SELECT DATE(validafter), COUNT(*) FROM ("
                    + "SELECT validafter FROM statusentry GROUP BY validafter"
                    + ") AS foo GROUP BY DATE(validafter) "
                    + "ORDER BY DATE(validafter)");
            while (rs.next()) {
                consensusesPerDay.put(rs.getString("date"), rs.getInt("count"));
            }
            rs.close();
            System.out.println("finished after "
                    + ((System.currentTimeMillis() - started) / 1000)
                    + " seconds.");
        }

        // how many running routers do we have?
        if (evaluation == -1 || evaluation == 1) {
            System.out.print("Counting relay flags... ");
            long started = System.currentTimeMillis();
            rs = s.executeQuery("SELECT DATE(validafter), "
                    + "SUM(CAST(exit AS INT)) AS exit, "
                    + "SUM(CAST(fast AS INT)) AS fast, "
                    + "SUM(CAST(guard AS INT)) AS guard, "
                    + "SUM(CAST(hsdir AS INT)) AS hsdir, "
                    + "SUM(CAST(named AS INT)) AS named, "
                    + "SUM(CAST(stable AS INT)) AS stable, "
                    + "SUM(CAST(running AS INT)) AS running, "
                    + "SUM(CAST(unnamed AS INT)) AS unnamed, "
                    + "SUM(CAST(valid AS INT)) AS valid, "
                    + "SUM(CAST(v2dir AS INT)) AS v2dir FROM statusentry "
                    + "WHERE running = '1' GROUP BY DATE(validafter) "
                    + "ORDER BY DATE(validafter)");
            File file = new File(outputDirectory.getAbsolutePath()
                    + File.separatorChar + "relayflags.csv");
            out = new BufferedWriter(new FileWriter(file, false));
            out.write("time,exit,fast,guard,hsdir,named,stable,running,"
                    + "unnamed,valid,v2dir\n");
            while (rs.next()) {
                int c = consensusesPerDay.get(rs.getString("date"));
                out.write(rs.getString("date") + ","
                        + (rs.getInt("exit") / c) + ","
                        + (rs.getInt("fast") / c) + ","
                        + (rs.getInt("guard") / c) + ","
                        + (rs.getInt("hsdir") / c) + ","
                        + (rs.getInt("named") / c) + ","
                        + (rs.getInt("stable") / c) + ","
                        + (rs.getInt("running") / c) + ","
                        + (rs.getInt("unnamed") / c) + ","
                        + (rs.getInt("valid") / c) + ","
                        + (rs.getInt("v2dir") / c) + "\n");
                if (rs.getString("date").equals("2008-10-24")) {
                        // terrible hack!!!
                    out.write("2008-10-25,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA\n");
                }
            }
            out.close();
            rs.close();
            System.out.println("finished after "
                    + ((System.currentTimeMillis() - started) / 1000)
                    + " seconds.");
        }

        // what platforms are routers running?
        if (evaluation == -1 || evaluation == 2) {
            System.out.print("Counting router platforms... ");
            long started = System.currentTimeMillis();
            rs = s.executeQuery("SELECT DATE(validafter), "
                    + "SUBSTR(descriptor.platform, 5, 5), "
                    + "COUNT(*) FROM statusentry NATURAL JOIN descriptor "
                    + "WHERE running = '1' GROUP BY DATE(validafter), "
                    + "SUBSTR(descriptor.platform, 5, 5) "
                    + "ORDER BY DATE(validafter)");
            File file = new File(outputDirectory.getAbsolutePath()
                    + File.separatorChar + "platforms.csv");
            out = new BufferedWriter(new FileWriter(file, false));
            out.write("time,v00,v010,v011,v012,v020,v021,v022,total\n");
            int v00 = 0, v010 = 0, v011 = 0, v012 = 0, v020 = 0, v021 = 0,
                    v022 = 0;
            String date = null;
            while (rs.next()) {
                if (date != null && !rs.getString("date").equals(date)) {
                    int c = consensusesPerDay.get(date);
                    out.write(date + "," + (v00 / c) + "," + (v010 / c) + "," + (v011 / c) + ","
                            + (v012 / c) + "," + (v020 / c) + "," + (v021 / c)
                            + "," + (v022 / c) + "," + ((v00 + v010 + v011 + v012
                            + v020 + v021 + v022) / c) + "\n");
                    v00 = 0;
                    v010 = 0;
                    v011 = 0;
                    v012 = 0;
                    v020 = 0;
                    v021 = 0;
                    v022 = 0;
                    if (date.equals("2008-10-24")) { // terrible hack!!!
                        out.write("2008-10-25,NA,NA,NA,NA,NA,NA,NA,NA\n");
                    }
                }
                date = rs.getString("date");
                String substr = rs.getString("substr");
                if (substr.startsWith("0.0")) {
                    v00 = rs.getInt("count");
                } else if (substr.equals("0.1.0")) {
                    v010 = rs.getInt("count");
                } else if (substr.equals("0.1.1")) {
                    v011 = rs.getInt("count");
                } else if (substr.equals("0.1.2")) {
                    v012 = rs.getInt("count");
                } else if (substr.equals("0.2.0")) {
                    v020 = rs.getInt("count");
                } else if (substr.equals("0.2.1")) {
                    v021 = rs.getInt("count");
                } else if (substr.equals("0.2.2")) {
                    v022 = rs.getInt("count");
                } else {
                    System.out.println("Unrecognized version: " + substr);
                }
            }
            int c = consensusesPerDay.get(date);
            out.write(date + "," + (v00 / c) + "," + (v010 / c) + "," + (v011 / c) + ","
                    + (v012 / c) + "," + (v020 / c) + "," + (v021 / c) + ","
                    + (v022 / c) + "," + ((v00 + v010 + v011 + v012 + v020 + v021
                    + v022) / c) + "\n");
            out.close();
            rs.close();
            System.out.println("finished after "
                    + ((System.currentTimeMillis() - started) / 1000)
                    + " seconds.");
        }

        // how much bandwidth do routers advertise? (1948 seconds)
        if (evaluation == -1 || evaluation == 3) {
            System.out.print("Summing up advertised bandwidth claims... ");
            long started = System.currentTimeMillis();
            // bandwidth lines
            rs = s.executeQuery("SELECT DATE(validafter), "
                    + "SUM(bandwidthavg) AS average, "
                    + "SUM(bandwidthburst) AS burst, "
                    + "SUM(bandwidthobserved) AS observed "
                    + "FROM statusentry NATURAL JOIN descriptor "
                    + "WHERE running = '1' "
                    + "GROUP BY DATE(validafter) "
                    + "ORDER BY DATE(validafter)");
            SortedMap<String, Long[]> bandwidths
                    = new TreeMap<String, Long[]>();
            while (rs.next()) {
                Long[] val = new Long[8];
                val[0] = rs.getLong("average");
                val[1] = rs.getLong("burst");
                val[2] = rs.getLong("observed");
                val[3] = 0L;
                val[4] = 0L;
                val[5] = 0L;
                val[6] = 0L;
                val[7] = 0L;
                bandwidths.put(rs.getString("date"), val);
            }
            System.out.print("bandwidth lines read... ");
            // relays indicating their bandwidth history in their router
            // descriptors
            rs = s.executeQuery("SELECT DATE(validafter), "
                    + "SUM(readhistoryavg) AS read, "
                    + "SUM(writehistoryavg) AS write, COUNT(*) "
                    + "FROM statusentry NATURAL JOIN descriptor "
                    + "WHERE running = '1' AND (readhistoryavg IS NOT NULL "
                    + "OR writehistoryavg IS NOT NULL) "
                    + "GROUP BY DATE(validafter) "
                    + "ORDER BY DATE(validafter)");
            while (rs.next()) {
                Long[] val = bandwidths.get(rs.getString("date"));
                val[3] += (rs.getLong("read") + rs.getLong("write")) / 2;
                val[4] += rs.getLong("count");
            }
            System.out.print("descriptor histories read... ");
            // relays indicating their bandwidth history in their extra-info
            // documents
            rs = s.executeQuery("SELECT DATE(validafter), "
                    + "SUM(readhistoryavgex) AS extraread, "
                    + "SUM(writehistoryavgex) AS extrawrite, COUNT(*) "
                    + "FROM statusentry NATURAL JOIN descriptor "
                    + "NATURAL JOIN extrainfo "
                    + "WHERE running='1' AND readhistoryavg IS NULL "
                    + "AND writehistoryavg IS NULL "
                    + "GROUP BY DATE(validafter) "
                    + "ORDER BY DATE(validafter)");
            while (rs.next()) {
                Long[] val = bandwidths.get(rs.getString("date"));
                val[5] += (rs.getLong("extraread") + rs.getLong("extrawrite"))
                        / 2;
                val[6] += rs.getLong("count");
            }
            System.out.print("extra-info histories read... ");
            // all relays (including those which do not indicate their
            // bandwidth history at all)
            rs = s.executeQuery("SELECT DATE(validafter), COUNT(*) "
                    + "FROM statusentry WHERE running = '1' "
                    + "GROUP BY DATE(validafter) ORDER BY DATE(validafter)");
            while (rs.next()) {
                Long[] val = bandwidths.get(rs.getString("date"));
                val[7] += rs.getLong("count");
            }
            System.out.print("number of status entries read... ");
            File file = new File(outputDirectory.getAbsolutePath()
                    + File.separatorChar + "advertised.csv");
            out = new BufferedWriter(new FileWriter(file, false));
            out.write("time,average,burst,observed,routerbw,routercnt,extrabw,"
                    + "extracnt,totalcnt\n");
            for (Map.Entry<String, Long[]> e : bandwidths.entrySet()) {
                Long[] val = e.getValue();
                String date = e.getKey();
                int c = consensusesPerDay.get(date);
                out.write(date + "," + (val[0] / c) + "," + (val[1] / c) + ","
                        + (val[2] / c) + "," + (val[3] / c) + ","
                        + (val[4] / c) + "," + (val[5] / c) + ","
                        + (val[6] / c) + "," + (val[7] / c) + "\n");
                if (date.equals("2008-10-24")) { // terrible hack!!!
                    out.write("2008-10-25,NA,NA,NA,NA,NA,NA,NA,NA\n");
                }
            }
            out.close();
            rs.close();
            System.out.println("finished after "
                    + ((System.currentTimeMillis() - started) / 1000)
                    + " seconds.");
        }

        // how many relays are running on dynamic IP addresses?
        if (evaluation == -1 || evaluation == 4) {
            System.out.print("Counting relays on dynamic IP addresses... ");
            long started = System.currentTimeMillis();
            rs = s.executeQuery("SELECT DATE(validafter), count AS ips, "
                    + "COUNT(*) FROM statusentry NATURAL JOIN descriptor "
                    + "NATURAL JOIN (SELECT fingerprint, COUNT(*) FROM ("
                    + "SELECT fingerprint FROM descriptor GROUP BY address, "
                    + "fingerprint) AS foo GROUP BY fingerprint) AS bar "
                    + "WHERE running = '1' GROUP BY DATE(validafter), ips "
                    + "ORDER BY DATE(validafter), ips");
            File file = new File(outputDirectory.getAbsolutePath()
                    + File.separatorChar + "dynamic.csv");
            out = new BufferedWriter(new FileWriter(file, false));
            out.write("time,ip1,ip2,ip3to5,ip6to10,ip11ormore,total\n");
            int ip1 = 0, ip2 = 0, ip3to5 = 0, ip6to10 = 0, ip11ormore = 0;
            SortedMap<Integer, Double> relaysSeen =
                    new TreeMap<Integer, Double>();
            int daysSeen = 1;
            String date = null;
            while (rs.next()) {
                if (date != null && !rs.getString("date").equals(date)) {
                    int c = consensusesPerDay.get(date);
                    out.write(date + "," + (ip1 / c) + "," + (ip2 / c) + ","
                            + (ip3to5 / c) + "," + (ip6to10 / c) + ","
                            + (ip11ormore / c) + ","
                            + ((ip1 + ip2 + ip3to5 + ip6to10 + ip11ormore)
                            / c) + "\n");
                    ip1 = 0;
                    ip2 = 0;
                    ip3to5 = 0;
                    ip6to10 = 0;
                    ip11ormore = 0;
                    daysSeen++;
                    if (date.equals("2008-10-24")) { // terrible hack!!!
                        out.write("2008-10-25,NA,NA,NA,NA,NA,NA\n");
                    }
                }
                date = rs.getString("date");
                int c = consensusesPerDay.get(date);
                if (relaysSeen.containsKey(rs.getInt("ips"))) {
                    relaysSeen.put(rs.getInt("ips"),
                            relaysSeen.remove(rs.getInt("ips"))
                            + rs.getDouble("count") / (double) c);
                } else {
                    relaysSeen.put(rs.getInt("ips"), rs.getDouble("count")
                            / (double) c);
                }
                switch (rs.getInt("ips")) {
                    case 1:
                        ip1 += rs.getInt("count");
                        break;
                    case 2:
                        ip2 += rs.getInt("count");
                        break;
                    case 3:
                    case 4:
                    case 5:
                        ip3to5 += rs.getInt("count");
                        break;
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        ip6to10 += rs.getInt("count");
                        break;
                    default:
                        ip11ormore += rs.getInt("count");
                        break;
                }
            }
            int c = consensusesPerDay.get(date);
            out.write(date + "," + (ip1 / c) + "," + (ip2 / c) + ","
                    + (ip3to5 / c) + "," + (ip6to10 / c) + ","
                    + (ip11ormore / c) + ","
                    + ((ip1 + ip2 + ip3to5 + ip6to10 + ip11ormore) / c)
                    + "\n");
            out.close();
            rs.close();
            file = new File(outputDirectory.getAbsolutePath()
                    + File.separatorChar + "dynamic-cutoff.csv");
            out = new BufferedWriter(new FileWriter(file, false));
            out.write("ips,count\n");
            for (int i = 1; i <= relaysSeen.lastKey(); i++) {
                if (relaysSeen.containsKey(i)) {
                    out.write(i + "," + (relaysSeen.get(i) / (double) daysSeen)
                            + "\n");
                } else {
                    out.write(i + ",0\n");
                }
            }
            out.close();
            System.out.println("finished after " + ((System.currentTimeMillis()
                    - started) / 1000) + " seconds.");
        }

        // where are relays located? (1578 seconds)
        if (evaluation == -1 || evaluation == 5) {
            System.out.print("Counting relays by country... ");
            long started = System.currentTimeMillis();
            rs = s.executeQuery("SELECT country FROM descriptor "
                    + "GROUP BY country");
            SortedSet<String> countries = new TreeSet<String>();
            while (rs.next()) {
                countries.add(rs.getString("country"));
            }
            File file = new File(outputDirectory.getAbsolutePath()
                    + File.separatorChar + "country.csv");
            out = new BufferedWriter(new FileWriter(file, false));
            out.write("time,");
            for (String f : countries) {
                out.write(f + ",");
            }
            out.write("total\n");
            rs = s.executeQuery("SELECT DATE(validafter), country, COUNT(*) "
                    + "FROM statusentry NATURAL JOIN descriptor "
                    + "WHERE running = '1' GROUP BY DATE(validafter), country "
                    + "ORDER BY DATE(validafter)");
            String date = null;
            Map<String, Double> nodesSeen = new HashMap<String, Double>();
            while (rs.next()) {
                if (date != null && !rs.getString("date").equals(date)) {
                    out.write(date + ",");
                    int c = consensusesPerDay.get(date);
                    double total = 0.0D;
                    for (String f : countries) {
                        if (nodesSeen.containsKey(f)) {
                            out.write((((int) nodesSeen.get(f).doubleValue())
                                    / c) + ",");
                            total += nodesSeen.get(f);
                        } else {
                            out.write("0,");
                        }
                    }
                    out.write((((int) total) / c) + "\n");
                    nodesSeen.clear();
                    if (date.equals("2008-10-24")) { // terrible hack!!!
                        out.write("2008-10-25,");
                        for (int i = 0; i < countries.size(); i++) {
                            out.write("NA,");
                        }
                        out.write("NA\n");
                    }
                }
                date = rs.getString("date");
                nodesSeen.put(rs.getString("country"), rs.getDouble("count"));
            }
            out.write(date + ",");
            int c = consensusesPerDay.get(date);
            double total = 0.0D;
            for (String f : countries) {
                if (nodesSeen.containsKey(f)) {
                    out.write((((int) nodesSeen.get(f).doubleValue()) / c)
                            + ",");
                    total += nodesSeen.get(f);
                } else {
                    out.write("0,");
                }
            }
            out.write((((int) total) / c) + "\n");
            out.close();
            rs.close();
            System.out.println("finished after " + ((System.currentTimeMillis()
                    - started) / 1000) + " seconds.");
        }

        // how much bandwidth do routers report in their histories per country?
        if (evaluation == -1 || evaluation == 6) {
            System.out.print("Summing up bandwidth histories per "
                    + "country... ");
            long started = System.currentTimeMillis();
            rs = s.executeQuery("SELECT country FROM descriptor "
                    + "GROUP BY country");
            SortedSet<String> countries = new TreeSet<String>();
            while (rs.next()) {
                countries.add(rs.getString("country"));
            }
            SortedMap<String, Map<String, Long[]>> bandwidths =
                    new TreeMap<String, Map<String, Long[]>>();
            // relays indicating their bandwidth history in their router
            // descriptors
            rs = s.executeQuery("SELECT DATE(validafter), country, "
                    + "SUM(readhistoryavg) AS read, "
                    + "SUM(writehistoryavg) AS write, COUNT(*) "
                    + "FROM statusentry NATURAL JOIN descriptor "
                    + "WHERE running = '1' AND (readhistoryavg IS NOT NULL "
                    + "OR writehistoryavg IS NOT NULL) "
                    + "GROUP BY DATE(validafter), country");
            while (rs.next()) {
                String date = rs.getString("date");
                long read = rs.getLong("read");
                long write = rs.getLong("write");
                String country = rs.getString("country");
                Map<String, Long[]> bwdate = bandwidths.get(date);
                if (bwdate == null) {
                    bwdate = new HashMap<String, Long[]>();
                    bandwidths.put(date, bwdate);
                }
                Long[] l = new Long[2];
                l[0] = (read + write) / 2;
                l[1] = 0L;
                bwdate.put(country, l);
            }
            System.out.print("descriptor histories read... ");
            // relays indicating their bandwidth history in their extra-info
            // documents
            rs = s.executeQuery("SELECT DATE(validafter), country, "
                    + "SUM(readhistoryavgex) AS extraread, "
                    + "SUM(writehistoryavgex) AS extrawrite "
                    + "FROM statusentry NATURAL JOIN descriptor "
                    + "NATURAL JOIN extrainfo "
                    + "WHERE running='1' AND readhistoryavg IS NULL "
                    + "AND writehistoryavg IS NULL "
                    + "GROUP BY DATE(validafter), country");
            while (rs.next()) {
                String date = rs.getString("date");
                long extraread = rs.getLong("extraread");
                long extrawrite = rs.getLong("extrawrite");
                String country = rs.getString("country");
                if (!bandwidths.containsKey(date)) {
                    Map<String, Long[]> n = new HashMap<String, Long[]>();
                    bandwidths.put(date, n);
                }
                Map<String, Long[]> bwdate = bandwidths.get(date);
                Long[] l = bwdate.get(country);
                if (l == null) {
                    l = new Long[2];
                    l[0] = 0L;
                    l[1] = (extraread + extrawrite) / 2;
                    //bwdate.put(rs.getString("country"), l);
                } else {
                    l[1] = (extraread + extrawrite) / 2;
                }
            }
            System.out.print("extra-info histories read... ");
            File file = new File(outputDirectory.getAbsolutePath()
                    + File.separatorChar + "countrybw.csv");
            out = new BufferedWriter(new FileWriter(file, false));
            out.write("time,");
            for (String f : countries) {
                out.write("r" + f + ",e" + f + ",t" + f + ",");
            }
            out.write("rtotal,etotal,ttotal\n");
            for (Map.Entry<String, Map<String, Long[]>> e
                    : bandwidths.entrySet()) {
                String date = e.getKey();
                Map<String, Long[]> byCountry = e.getValue();
                out.write(date + ",");
                long c = (long) consensusesPerDay.get(date);
                long rtotal = 0L, etotal = 0L, ttotal = 0L;
                for (String f : countries) {
                    if (byCountry.containsKey(f)) {
                        out.write(((byCountry.get(f)[0].longValue()) / c)
                                + ",");
                        out.write(((byCountry.get(f)[1].longValue()) / c)
                                + ",");
                        out.write(((byCountry.get(f)[0].longValue()
                                + byCountry.get(f)[1].longValue()) / c) + ",");
                        rtotal += byCountry.get(f)[0];
                        etotal += byCountry.get(f)[1];
                        ttotal += byCountry.get(f)[0] + byCountry.get(f)[1];
                    } else {
                        out.write("0,0,0,");
                    }
                }
                out.write((rtotal / c) + "," + (etotal / c) + ","
                        + (ttotal / c) + "\n");
                if (date.equals("2008-10-24")) { // terrible hack!!!
                    out.write("2008-10-25,");
                    for (int i = 0; i < countries.size(); i++) {
                        out.write("NA,NA,NA,");
                    }
                    out.write("NA,NA,NA\n");
                }
            }
            out.close();
            rs.close();
            System.out.println("finished after " + ((System.currentTimeMillis()
                    - started) / 1000) + " seconds.");
        }

        // how much bandwidth do routers advertise per country?
        if (evaluation == -1 || evaluation == 7) {
            System.out.print("Summing up advertised bandwidth claims per "
                    + "country... ");
            long started = System.currentTimeMillis();
            rs = s.executeQuery("SELECT country FROM descriptor "
                    + "GROUP BY country");
            SortedSet<String> countries = new TreeSet<String>();
            while (rs.next()) {
                countries.add(rs.getString("country"));
            }
            File file = new File(outputDirectory.getAbsolutePath()
                    + File.separatorChar + "countryadvbw.csv");
            out = new BufferedWriter(new FileWriter(file, false));
            out.write("time,");
            for (String f : countries) {
                out.write("a" + f + ",b" + f + ",o" + f + ",");
            }
            out.write("atotal,btotal,ototal\n");
            rs = s.executeQuery("SELECT DATE(validafter), country, "
                    + "SUM(bandwidthavg) AS average, "
                    + "SUM(bandwidthburst) AS burst, "
                    + "SUM(bandwidthobserved) AS observed "
                    + "FROM statusentry NATURAL JOIN descriptor "
                    + "WHERE running = '1' GROUP BY DATE(validafter), country "
                    + "ORDER BY DATE(validafter)");
            String date = null;
            Map<String, Double[]> nodesSeen = new HashMap<String, Double[]>();
            while (rs.next()) {
                if (date != null && !rs.getString("date").equals(date)) {
                    out.write(date);
                    double c = (double) consensusesPerDay.get(date);
                    double[] total = new double[3];
                    for (String f : countries) {
                        if (nodesSeen.containsKey(f)) {
                            Double[] vals = nodesSeen.get(f);
                            for (int i = 0; i < 3; i++) {
                                out.write("," + (int) (vals[i].doubleValue()
                                    / c));
                                total[i] += vals[i].doubleValue();
                            }
                        } else {
                            out.write(",0,0,0");
                        }
                    }
                    for (int i = 0; i < 3; i++) {
                        out.write("," + ((int) (total[i] / c)));
                    }
                    out.write("\n");
                    nodesSeen.clear();
                    if (date.equals("2008-10-24")) { // terrible hack!!!
                        out.write("2008-10-25");
                        for (int i = 0; i < countries.size(); i++) {
                            out.write(",NA,NA,NA");
                        }
                        out.write(",NA,NA,NA\n");
                    }
                }
                date = rs.getString("date");
                Double[] vals = new Double[3];
                vals[0] = rs.getDouble("average");
                vals[1] = rs.getDouble("burst");
                vals[2] = rs.getDouble("observed");
                nodesSeen.put(rs.getString("country"), vals);
            }
            out.write(date);
            int c = consensusesPerDay.get(date);
            double[] total = new double[3];
            for (String f : countries) {
                if (nodesSeen.containsKey(f)) {
                    Double[] vals = nodesSeen.get(f);
                    for (int i = 0; i < 3; i++) {
                        out.write("," + (int) (vals[i].doubleValue()
                            / c));
                        total[i] += vals[i].doubleValue();
                    }
                } else {
                    out.write(",0,0,0");
                }
            }
            for (int i = 0; i < 3; i++) {
                out.write("," + ((int) (total[i] / c)));
            }
            out.write("\n");
            out.close();
            rs.close();
            System.out.println("finished after " + ((System.currentTimeMillis()
                    - started) / 1000) + " seconds.");
        }

        // disconnect from database
        conn.close();
    }
}

