package org.torproject.metrics.autopromotion

import _root_.java.io._
import _root_.java.text.SimpleDateFormat
import _root_.java.util.Date
import _root_.scala.collection.immutable._


class Status(mRunningRouters: Set[AutoPromotion.RouterName]) {
  override def toString = runningRouters.size.toString + " running routers"
  def runningRouters = mRunningRouters
}

object RouterInfo {
  var totalSamples = -1
}

class RouterInfo {
  var lastDate = 0L
  var samplesRunning = 0

  def addDate(d: Date) {
    assert (d.getTime > lastDate)
    lastDate = d.getTime
    samplesRunning += 1
  }

  def isUseful = (samplesRunning.toDouble/RouterInfo.totalSamples.toDouble*100) >= 90.0

  override def toString = {
    val percent = samplesRunning.toDouble/RouterInfo.totalSamples.toDouble*100
    "Running %d out of %d (%5.5f%%)".format(samplesRunning, RouterInfo.totalSamples, percent)
  }
}

object AutoPromotion {

  type RouterName = String

  /** Allow Java comparable objects to be put into a TreeMap
    * From: http://thread.gmane.org/gmane.comp.lang.scala/16102 */
  implicit def comparable2ordered[A <: Comparable[A]](x: A): Ordered[A] =
    new Ordered[A] with Proxy {
      val self = x
      def compare(y: A): Int = { x.compareTo(y) }
  }

  def debug(message: String) =
    System.out.println(message)

  /** Return a list of all files recursively found in <root> */
  def getAllFiles(root: File): List[File] = {
    var allFiles: List[File] = Nil
    for (f <- root.listFiles) {
      if (f.isDirectory)
        allFiles = getAllFiles(f) ::: allFiles
      else
        allFiles = f :: allFiles
    }
    allFiles
  }

  /** Parse a consensus, and return a set of running routers */
  def parseStatus(f: File) = {
    def parseStatus(br: BufferedReader, acc: Set[RouterName]): Set[RouterName] = {
      val line = br.readLine
      if (null == line)
        acc
      else if (!line.startsWith("r "))
        parseStatus(br, acc)
      else
        parseStatus(br, acc + line.split(" ")(2))
    }
    debug("Reading " + f)
    val br = new BufferedReader(new FileReader(f))
    parseStatus(br, Set.empty)
  }

  /** Given a list of Files, return a mapping of Date -> Status */
  def getRunningNodes(allFiles: List[File]) = {
    val parseFormat = new SimpleDateFormat("yyyyMMdd-HHmmss")

    def getRunningNodes(allFiles: List[File], acc: SortedMap[Date, Status]): SortedMap[Date, Status] = {
      allFiles match {
        case Nil => acc
        case f :: fs => {
          val fn = f.getName
          val timestamp = parseFormat.parse(fn.substring(0, 15))
          val runningRouters = parseStatus(f)
          getRunningNodes(fs, acc + (timestamp -> new Status(runningRouters)))
        }
      }
    }
    getRunningNodes(allFiles, TreeMap.empty)
  }

  def invertMapping(runningNodes: Map[Date, Status]) = {
    runningNodes.foldLeft[Map[RouterName, RouterInfo]](TreeMap.empty){
      (acc: Map[RouterName, RouterInfo],  ds: (Date, Status)) =>
        val (date, status) = ds
        status.runningRouters.toList.foldLeft(acc){(ri, n) =>
          if (ri.contains(n)) {
            val routerInfo = ri(n)
            routerInfo.addDate(date)
            ri
          } else {
            val routerInfo = new RouterInfo
            routerInfo.addDate(date)
            ri + (n -> routerInfo)
          }
       }
    }
  }

  type DateInfo = (Map[Date, Accuracy], Map[RouterName, RouterInfo])
  case class Accuracy(usefulNodesSize: Int, intersectionSize: Int, toDateSize: Int) {
    override def toString = {
      val falsePositives = toDateSize - intersectionSize
      val falseNegatives = usefulNodesSize - intersectionSize
      "Nodes %d (so far)/%d (total), false positives: %5.5f%%, false negatives: %5.5f%%".format(
        toDateSize, usefulNodesSize,
        falsePositives.toDouble/toDateSize*100,
        falseNegatives.toDouble/usefulNodesSize*100)
    }
  }

  def checkAccuracy(usefulNodes: Set[RouterName], runningNodes: Map[Date, Status]): Map[Date, Accuracy] = {
    RouterInfo.totalSamples = 0
    val usefulNodesSize = usefulNodes.size
    runningNodes.foldLeft[DateInfo]((TreeMap.empty[Date, Accuracy], TreeMap.empty[RouterName, RouterInfo])){
      (acc: DateInfo, ds: (Date, Status)) =>
      val (date, status) = ds
      val (accuracy, nodeInfo) = acc

      // newNodeInfo contains information up to and including the current date
      val newNodeInfo = status.runningRouters.toList.foldLeft(nodeInfo){(ri, n) =>
          if (ri.contains(n)) {
            val routerInfo = ri(n)
            routerInfo.addDate(date)
            ri
          } else {
            val routerInfo = new RouterInfo
            routerInfo.addDate(date)
            ri + (n -> routerInfo)
          }
      }

      RouterInfo.totalSamples = RouterInfo.totalSamples + 1

      // Find supposedly reliable nodes (keySet is not immutable, so make an immutable set from it)
      val usefulNodesToDate = Set.empty ++ newNodeInfo.filter{kv => kv._2.isUseful}.keySet

      // Check intersection with usefulNodes
      val toDateSize = usefulNodesToDate.size
      val intersectionSize = usefulNodesToDate.intersect(usefulNodes).size

      // Calculate accuracy
      val usefulnessAccuracy = Accuracy(usefulNodesSize, intersectionSize, toDateSize)

      (accuracy + (date -> usefulnessAccuracy), newNodeInfo)
    }._1
  }

  def main(args: Array[String]) {
    val inDir = new File(args(0)) // Directory to search
    val allFiles = getAllFiles(inDir) // All files to read
    val runningNodes = getRunningNodes(allFiles) // Mapping of date -> running nodes

    RouterInfo.totalSamples = allFiles.size
    val nodeInfo = invertMapping(runningNodes) // Mapping of node -> node information
    for ((k,v) <- nodeInfo)
      System.out.println(k + " -- " + v)

    // Find out which nodes are considered useful
    val usefulNodes = Set.empty ++ nodeInfo.filter{kv => kv._2.isUseful}.keySet
    System.out.println("Useful nodes")
    for (k <- usefulNodes)
      System.out.println(k)

    // For each time, how right are we?
    val accuracyByDate = checkAccuracy(usefulNodes, runningNodes)
    for ((k,v) <- accuracyByDate)
      System.out.println(k + " -- " + v)
  }
}

// vim: set ts=2 sw=2 tw=0 et:
