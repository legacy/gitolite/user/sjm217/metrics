/* Copyright 2009 Karsten Loesing
 * See LICENSE for licensing information */
package org.torproject.metrics.buffer;

import java.io.*;
import java.text.*;
import java.util.*;

public final class EvaluateCellStats {

  private EvaluateCellStats() {
  }

  public static void main(final String[] args) throws Exception {

    if (args.length != 2) {
      System.err.println("Usage: java "
          + EvaluateCellStats.class.getSimpleName()
          + " <cell-stats directory> <output directory>");
      System.exit(1);
    }
    File cellStatsDirectory = new File(args[0]);
    if (!cellStatsDirectory.exists()
        || !cellStatsDirectory.isDirectory()) {
      System.err.println("Cell stats directory '"
          + cellStatsDirectory.getAbsolutePath()
          + "' does not exist or is not a directory.");
      System.exit(1);
    }
    File outputDirectory = new File(args[1]);
    if (outputDirectory.exists() && !outputDirectory.isDirectory()) {
      System.err.println("Output directory '"
          + outputDirectory.getAbsolutePath()
          + "' exists, but is not a directory.");
      System.exit(1);
    }
    outputDirectory.mkdir();

    long started = System.currentTimeMillis();
    SortedSet<String> outLines = new TreeSet<String>(
        new Comparator<String>() {
      public int compare(String a, String b) {
        return a.compareToIgnoreCase(b);
      }
    });
    Stack<File> filesLeftToParse = new Stack<File>();
    filesLeftToParse.push(cellStatsDirectory);
    while (!filesLeftToParse.isEmpty()) {
      File directoryOrFile = filesLeftToParse.pop();
      if (directoryOrFile.isDirectory()) {
        for (File fileInDir : directoryOrFile.listFiles()) {
          filesLeftToParse.push(fileInDir);
        }
        continue;
      }
      BufferedReader br = new BufferedReader(new FileReader(
          directoryOrFile));
      String line = null;
      StringBuilder out = new StringBuilder();
      while ((line = br.readLine()) != null) {
        if (line.startsWith("written")) {
          out.append(directoryOrFile.getName() + "," + line.split(" ")[1]
              + " " + line.split(" ")[2]);
        } else if (line.startsWith("processed-cells ")) {
          out.append("," + line.split(" ")[1]);
        } else if (line.startsWith("queued-cells ")) {
          out.append("," + line.split(" ")[1]);
        } else if (line.startsWith("time-in-queue ")) {
          out.append("," + line.split(" ")[1]);
        } else if (line.startsWith("number-of-circuits-per-share ")) {
          out.append("," + line.split(" ")[1] + "\n");
          outLines.add(out.toString());
          out = new StringBuilder();
        }
      }
    }

    BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
        outputDirectory.getAbsolutePath() + File.separatorChar
        + "cell.csv"), false));
    bw.write("nickname,date,p0,p1,p2,p3,p4,p5,p6,p7,p8,p9,q0,q1,q2,q3,q4,"
        + "q5,q6,q7,q8,q9,t0,t1,t2,t3,t4,t5,t6,t7,t8,t9,num\n");
    for (String outLine : outLines) {
      bw.write(outLine);
    }
    bw.close();
    System.out.println("Evaluation finished after "
        + (System.currentTimeMillis() - started) / 1000 + " seconds.");
  }
}

